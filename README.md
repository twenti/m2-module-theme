# Magento 2 rapid theme development

This module provides some tools for frontend development.

  * Sass Preprocessor


## Compatibility
  * Magento Community Edition or Enterprise Edition 2.2.x-2.3.x



## Installing via composer
  * Open command line
  * Using command "cd" navigate to your Magento 2 root directory
  * Run commands:
    * ```composer require twenti/m2-module-theme```
    * ```php bin/magento setup:upgrade```
    * ```php bin/magento setup:di:compile```
    * ```php bin/magento setup:static-content:deploy```



## Support
For any issues, please open a support ticket in our customer's area
[issue tracker](https://support.twenti.io/).

## Need More Features?
Please contact us to get a quote
https://twenti.io/contact

## Other extensions by Twenti
  * Coming soon
