<?php

namespace Twenti\Theme\Plugin\Sass\PreProcessor;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 * @see       https://github.com/magento/magento2/issues/6943
 */

use Magento\Developer\Console\Command\SourceThemeDeployCommand;


class Chain
{

    /**
     * @var \Magento\Developer\Console\Command\SourceThemeDeployCommand
     */
    private $_sourceThemeDeployCommand;
    /**
     * @var \Twenti\Theme\Model\Sass\PreProcessor\File
     */
    private $_filePreprocessor;
    /**
     * @var \Twenti\Theme\Model\Sass\PreProcessor\Cli
     */
    private $_cliPreprocessor;

    public function __construct(
        \Magento\Developer\Console\Command\SourceThemeDeployCommand $sourceThemeDeployCommand,
        \Twenti\Theme\Model\Sass\PreProcessor\File $filePreprocessor,
        \Twenti\Theme\Model\Sass\PreProcessor\Cli $cliPreprocessor
    ) {
        $this->_sourceThemeDeployCommand = $sourceThemeDeployCommand;
        $this->_filePreprocessor         = $filePreprocessor;
        $this->_cliPreprocessor          = $cliPreprocessor;
    }

    /**
     * @param  Magento\Framework\View\Asset\PreProcessor\Chain  $subject
     * @param  <type>                                           $result
     * @return boolean
     */
    public function afterIsChanged( \Magento\Framework\View\Asset\PreProcessor\Chain $subject, $result )
    {
        if ( ! $this->_cliPreprocessor->isCli() || ! $this->_cliPreprocessor->isCommand( 'dev:source-theme:deploy' ) ) {
            return $result;
        }

        if ( ! $this->isEntryFile( $subject->getAsset()->getFilePath() ) ) {
            return false;
        }

        return $result;
    }

    /**
     * Determines if entry file
     *
     * @param  string   $path
     * @return boolean
     */
    private function isEntryFile( string $path )
    {
        $input       = $this->_cliPreprocessor->getInput( $this->_sourceThemeDeployCommand->getDefinition() );
        $files       = $input->getArgument( SourceThemeDeployCommand::FILE_ARGUMENT );
        $contentType = $input->getOption( SourceThemeDeployCommand::TYPE_ARGUMENT );

        foreach ( $files as $file ) {
            if ( ( $file === $path ) || ( $this->_filePreprocessor->fixFileExtension( $file, $contentType ) === $path ) ) {
                return true;
            }
        }

        return false;
    }

}