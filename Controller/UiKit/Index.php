<?php

namespace Twenti\Theme\Controller\UiKit;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;


class Index extends Action
{

	/**
	 * @var    \Magento\Framework\App\State
	 */
	protected $_appState;
	/**
	 * @var    \Magento\Framework\Controller\Result\ForwardFactory
	 */
	protected $_resultForwardFactory;
	/**
	 * @var    \Magento\Framework\View\Result\PageFactory
	 */
	protected $_resultPageFactory;

	/**
	 * Constructor
	 *
	 * @param  \Magento\Framework\App\Action\Context                $context
	 * @param  \Magento\Framework\App\State                         $appState
	 * @param  \Magento\Framework\Controller\Result\ForwardFactory  $resultForwardFactory
	 * @param  \Magento\Framework\View\Result\PageFactory           $resultPageFactory
	 */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\App\State $appState,
		\Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	)
	{
		$this->_appState             = $appState;
		$this->_resultForwardFactory = $resultForwardFactory;
		$this->_resultPageFactory    = $resultPageFactory;

		parent::__construct( $context );
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute()
	{
		if ( $this->_appState->getMode() != \Magento\Framework\App\State::MODE_DEVELOPER ) {
			$result = $this->_resultForwardFactory->create();

			return $result->forward( 'noroute' );
		}

		$resultPage = $this->_resultPageFactory->create();

		$resultPage->getConfig()->getTitle()->set( __( 'UI kit' ) );

		return $resultPage;
	}

}
