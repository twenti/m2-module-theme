<?php

namespace Twenti\Theme\Model\Sass\PreProcessor;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Symfony\Component\Console\Input\InputDefinition;


class Cli
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;
    /**
     * @var \Symfony\Component\Console\Input\ArgvInputFactory
     */
    protected $_argvInputFactory;
    /**
     * @var \Symfony\Component\Console\Input\ArgvInput
     */
    protected $_input;

    /**
     * Constructor
     *
     * @param  \Magento\Framework\App\RequestInterface            $request
     * @param  \Symfony\Component\Console\Input\ArgvInputFactory  $argvInputFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Symfony\Component\Console\Input\ArgvInputFactory $argvInputFactory
    ) {
        $this->_request          = $request;
        $this->_argvInputFactory = $argvInputFactory;
    }

    /**
     * Gets the input
     *
     * @param  \Symfony\Component\Console\Input\InputDefinition  $definition
     * @return \Symfony\Component\Console\Input\ArgvInput
     */
    public function getInput( InputDefinition $definition )
    {
        if ( $this->_input === null ) {
            $this->_input = $this->_argvInputFactory->create();

            $this->_input->bind( $definition );
        }

        return $this->_input;
    }

    /**
     * Determines if cli
     *
     * @return boolean
     */
    public function isCli()
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * Determines if command
     *
     * @param  string   $name
     * @return boolean
     */
    public function isCommand( $name )
    {
        return in_array( $name, $this->_request->getServerValue( 'argv', [] ) );
    }

}
