<?php

namespace Twenti\Theme\Model\Sass\PreProcessor;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\View\Asset\File\NotFoundException;


class File
{

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $_ioFile;

    /**
     * Constructor
     *
     * @param  \Magento\Framework\Filesystem\Io\File  $ioFile
     */
    public function __construct(
        \Magento\Framework\Filesystem\Io\File $ioFile
    ) {
        $this->_ioFile = $ioFile;
    }

    /**
     * Fix file extension
     *
     * @param  string  $path
     * @param  string  $extension
     * @return string
     */
    public function fixFileExtension( string $path, string $extension )
    {
        $info = $this->_ioFile->getPathInfo( $path );

        if ( ! isset( $info['extension'] ) ) {
            $path .= '.' . $extension;
        }

        return $path;
    }

    /**
     * Gets the underscore notation
     *
     * @param  string  $path
     * @return string
     */
    public function getUnderscoreNotation( string $path )
    {
        $info = $this->_ioFile->getPathInfo( $path );

        return $info['dirname'] . '/_' . $info['basename'];
    }

    /**
     * Checks if asset file exists or not
     *
     * @param  \Magento\Framework\View\Asset\File  $asset
     * @return boolean
     */
    public function assetFileExists( \Magento\Framework\View\Asset\File $asset )
    {
        try {
            $asset->getSourceFile();
        }
        catch ( NotFoundException $e ) {
            return false;
        }

        return true;
    }

    /**
     * Determines if the asset is a partial
     *
     * @param  string   $path
     * @return boolean
     */
    public function isPartial( string $path )
    {
        $info = $this->_ioFile->getPathInfo( $path );

        return ! isset( $info['basename'][0] ) ? false : ( $info['basename'][0] === '_' );
    }

    /**
     * Reads entire file into an array
     *
     * @param  string  $path
     * @param  string  $extension
     * @return array
     */
    public function readFileAsArray( string $path, string $extension = null )
    {
        $result = @file( $path );

        if ( ! $result && $extension ) {
            return $this->readFileAsArray( $path . '.' . $extension );
        }

        return $result ?: [];
    }

}
