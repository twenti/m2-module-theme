<?php

namespace Twenti\Theme\Model\Sass\PreProcessor\Instruction;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\View\Asset\File\FallbackContext;
use Magento\Framework\View\Asset\LocalInterface;


class MagentoImport implements \Magento\Framework\View\Asset\PreProcessorInterface
{

    /**
     * @var \Magento\Framework\View\DesignInterface
     */
    protected $_design;
    /**
     * @var \Magento\Framework\View\File\CollectorInterface
     */
    protected $_fileSource;
    /**
     * @var \Magento\Framework\Css\PreProcessor\ErrorHandlerInterface
     */
    protected $_errorHandler;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepository;
    /**
     * @var \Magento\Framework\View\Design\Theme\ThemeProviderInterface
     */
    protected $_themeProvider;
    /**
     * @var \Twenti\Theme\Model\Sass\PreProcessor\File
     */
    protected $_filePreprocessor;

    /**
     * Constructor
     *
     * @param  \Magento\Framework\View\DesignInterface                      $design
     * @param  \Magento\Framework\View\File\CollectorInterface              $fileSource
     * @param  \Magento\Framework\Css\PreProcessor\ErrorHandlerInterface    $errorHandler
     * @param  \Magento\Framework\View\Asset\Repository                     $assetRepository
     * @param  \Magento\Framework\View\Design\Theme\ThemeProviderInterface  $themeProvider
     * @param  \Twenti\Theme\Model\Sass\PreProcessor\File                   $filePreprocessor
     */
    public function __construct(
        \Magento\Framework\View\DesignInterface $design,
        \Magento\Framework\View\File\CollectorInterface $fileSource,
        \Magento\Framework\Css\PreProcessor\ErrorHandlerInterface $errorHandler,
        \Magento\Framework\View\Asset\Repository $assetRepository,
        \Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider,
        \Twenti\Theme\Model\Sass\PreProcessor\File $filePreprocessor
    ) {
        $this->_design           = $design;
        $this->_fileSource       = $fileSource;
        $this->_errorHandler     = $errorHandler;
        $this->_assetRepository  = $assetRepository;
        $this->_themeProvider    = $themeProvider;
        $this->_filePreprocessor = $filePreprocessor;
    }

    /**
     * @inheritdoc
     */
    public function process(\Magento\Framework\View\Asset\PreProcessor\Chain $chain)
    {
        $asset           = $chain->getAsset();
        $contentType     = $chain->getContentType();
        $replaceCallback = function( $matchContent ) use ( $asset, $contentType ) {
            return $this->replace( $matchContent, $asset, $contentType );
        };

        $chain->setContent( preg_replace_callback(
            \Magento\Framework\Css\PreProcessor\Instruction\MagentoImport::REPLACE_PATTERN,
            $replaceCallback,
            $chain->getContent()
        ));
    }

    /**
     * Replace
     *
     * @param  array                                         $matchedContent
     * @param  \Magento\Framework\View\Asset\LocalInterface  $asset
     * @param  <type>                                        $contentType
     * @return string
     */
    private function replace( array $matchedContent, LocalInterface $asset, $contentType )
    {
        $imports = [];

        try {
            $matchedFileId = $matchedContent['path'];

            if ( ! $this->_filePreprocessor->isPartial( $matchedFileId ) ) {
                $matchedFileId = $this->_filePreprocessor->getUnderscoreNotation( $matchedFileId );
            }

            $matchedFileId = $this->_filePreprocessor->fixFileExtension( $matchedFileId, $contentType );
            $relatedAsset  = $this->_assetRepository->createRelated( $matchedFileId, $asset );
            $resolvedPath  = $relatedAsset->getFilePath();
            $files         = $this->_fileSource->getFiles( $this->getTheme( $relatedAsset ), $resolvedPath );

            foreach ( $files as $file ) {
                $imports[] = $file->getModule()
                    ? "@import '{$file->getModule()}::{$resolvedPath}';"
                    : "@import '{$matchedFileId}';";
            }

        }
        catch ( \Exception $e ) {
            $this->_errorHandler->processException( $e );
        }

        return implode( "\n", $imports );
    }

    /**
     * Returns the theme
     *
     * @param  \Magento\Framework\View\Asset\LocalInterface  $asset
     * @return string
     */
    private function getTheme( LocalInterface $asset )
    {
        $context = $asset->getContext();

        if ( $context instanceof FallbackContext ) {
            return $this->_themeProvider->getThemeByFullPath(
                $context->getAreaCode() . '/' . $context->getThemePath()
            );
        }

        return $this->_design->getDesignTheme();
    }

}
