<?php

namespace Twenti\Theme\Model\Sass\PreProcessor\Instruction;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\View\Asset\LocalInterface;


class Import extends \Magento\Framework\Css\PreProcessor\Instruction\Import
{

	/**
	 * @var \Magento\Framework\View\Asset\Repository
	 */
	protected $_assetRepository;
	/**
	 * @var \Twenti\Theme\Model\Sass\PreProcessor\File
	 */
	protected $_filePreprocessor;

	/**
	 * Constructor
	 *
	 * @param  \Magento\Framework\View\Asset\NotationResolver\Module               $notationResolver
	 * @param  \Magento\Framework\Css\PreProcessor\FileGenerator\RelatedGenerator  $relatedFileGenerator
	 * @param  \Magento\Framework\View\Asset\Repository                            $assetRepository
	 * @param  \Twenti\Theme\Model\Sass\PreProcessor\File                          $filePreprocessor
	 */
	public function __construct(
		\Magento\Framework\View\Asset\NotationResolver\Module $notationResolver,
		\Magento\Framework\Css\PreProcessor\FileGenerator\RelatedGenerator $relatedFileGenerator,
		\Magento\Framework\View\Asset\Repository $assetRepository,
		\Twenti\Theme\Model\Sass\PreProcessor\File $filePreprocessor
	) {
		parent::__construct( $notationResolver, $relatedFileGenerator );

		$this->_assetRepository  = $assetRepository;
		$this->_filePreprocessor = $filePreprocessor;
	}

	/**
	 * @inheritdoc
	 */
	protected function replace(array $matchedContent, LocalInterface $asset, $contentType)
	{
		$matchedFileId = $this->_filePreprocessor->fixFileExtension( $matchedContent['path'], $contentType );
		$relatedAsset  = $this->_assetRepository->createRelated( $matchedFileId, $asset );

		if ( $this->_filePreprocessor->assetFileExists( $relatedAsset ) ) {
			return parent::replace( $matchedContent, $asset, $contentType );
		}

		$matchedContent['path'] = $this->_filePreprocessor->getUnderscoreNotation( $matchedContent['path'] );

		return parent::replace( $matchedContent, $asset, $contentType );
	}

}