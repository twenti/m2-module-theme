<?php

namespace Twenti\Theme\Model\Sass\Adapter;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\State;
use Magento\Framework\Phrase;
use Magento\Framework\View\Asset\ContentProcessorException;
use Magento\Framework\View\Asset\ContentProcessorInterface;
use Magento\Framework\View\Asset\File;
use Leafo\ScssPhp\Exception\ParserException;
use Leafo\ScssPhp\Exception\CompilerException;


class Processor implements ContentProcessorInterface
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;
    /**
     * @var \Magento\Framework\App\State
     */
    private $_appState;
    /**
     * @var \Magento\Framework\View\Asset\Source
     */
    private $_assetSource;
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $_directoryList;
    /**
     * @var \Magento\Framework\Css\PreProcessor\Config
     */
    private $_config;
    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $_ioFile;
    /**
     * @var \Leafo\ScssPhp\CompilerFactory
     */
    private $_compilerFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\View\Asset\Source $assetSource,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Css\PreProcessor\Config $config,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        \Leafo\ScssPhp\CompilerFactory $compilerFactory
    ) {
        $this->_logger          = $logger;
        $this->_appState        = $appState;
        $this->_assetSource     = $assetSource;
        $this->_directoryList   = $directoryList;
        $this->_config          = $config;
        $this->_ioFile          = $ioFile;
        $this->_compilerFactory = $compilerFactory;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\View\Asset\ContentProcessorException
     */
    public function processContent( File $asset )
    {
        $path = $asset->getPath();

        try {
            $developer = $this->_appState->getMode() === State::MODE_DEVELOPER;
            /** @var \Leafo\ScssPhp\Compiler $compiler */
            $compiler  = $this->_compilerFactory->create();

            if ( ! $developer ) {
                $compiler->setFormatter( \Leafo\ScssPhp\Formatter\Compressed::class );
            }

            $compiler->setImportPaths([
                $this->_directoryList->getPath( DirectoryList::VAR_DIR ) .
                '/' . $this->_config->getMaterializationRelativePath() .
                '/' . $this->_ioFile->dirname( $path )
            ]);

            $content = $this->_assetSource->getContent( $asset );

            if ( trim( $content ) === '' ) {
                return '';
            }

            $this->_logger->info( 'Compiling source file: ' . $path );

            gc_disable();

            $content = $compiler->compile( $content );

            gc_enable();

            if ( trim( $content ) === '' ) {
                $this->_logger->warning( 'Parsed file is empty: ' . $path );

                return '// --- empty ---';
            }

            return $content;
        }
        catch ( ParserException $e ) {
            $this->_logger->critical( $e->getMessage(), [ $path ] );

            return '// --- fatal error ---';
        }
        catch ( CompilerException $e ) {
            $this->_logger->critical( $e->getMessage(), [ $path ] );

            return '// --- fatal error ---';
        }
        catch ( \Exception $e ) {
            throw new ContentProcessorException( new Phrase( $e->getMessage() ) );
        }
    }

}
