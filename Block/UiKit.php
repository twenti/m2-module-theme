<?php

namespace Twenti\Theme\Block;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\View\Element\Template;


class UiKit extends Template
{

	/**
	 * @var string
	 */
	protected $_template = 'Twenti_Theme::ui-kit.phtml';

	/**
	 * Gets the block from request
	 *
	 * @return null|AbstractBlock
	 */
	public function getChildBlockFromRequest()
	{
		if (
			( $block = $this->getRequest()->getParam( 'block', false ) ) &&
			( $block = $this->getChildBlock( $block ) )
		) {
			return $block;
		}
	}

	/**
	 * Gets the child blocks
	 *
	 * @return array
	 */
	public function getChildBlocks() : array
	{
		return $this->getLayout()->getChildBlocks( $this->getNameInLayout() );
	}

}
