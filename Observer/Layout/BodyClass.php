<?php

namespace Twenti\Theme\Observer\Layout;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_Theme
 */

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Page\Config;


class BodyClass implements ObserverInterface
{

	const PREFIX_STORE   = 'store-';
	const PREFIX_WEBSITE = 'website-';
	const PREFIX_LANG    = 'lang-';

	/**
	 * @var \Magento\Framework\View\Page\Config
	 */
	protected $_pageConfig;
	/**
	 * @var \Magento\Framework\Locale\ResolverInterface
	 */
	protected $_localeResolver;
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * Constructor
	 *
	 * @param  \Magento\Framework\View\Page\Config          $pageConfig
	 * @param  \Magento\Framework\Locale\ResolverInterface  $localeResolver
	 * @param  \Magento\Framework\Locale\ResolverInterface  $storeManager
	 */
	public function __construct(
		\Magento\Framework\View\Page\Config $pageConfig,
		\Magento\Framework\Locale\ResolverInterface $localeResolver,
		\Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		$this->_pageConfig     = $pageConfig;
		$this->_localeResolver = $localeResolver;
		$this->_storeManager   = $storeManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute( \Magento\Framework\Event\Observer $observer )
	{
		// Store
		if ( $store = $this->_storeManager->getStore() ) {
			$this->_pageConfig->addBodyClass( $this->cleanCssClass( self::PREFIX_STORE . $store->getCode() ) );
		}

		// Website
		if ( $website = $this->_storeManager->getWebsite() ) {
			$this->_pageConfig->addBodyClass( $this->cleanCssClass( self::PREFIX_WEBSITE . $website->getCode() ) );
		}

		// Locale
		if ( $locale = $this->_localeResolver->getLocale() ) {
			if ( strpos( $locale, '_' ) ) {
				$this->_pageConfig->addBodyClass( $this->cleanCssClass( self::PREFIX_LANG . explode( '_', $locale )[0] ) );
			}

			$this->_pageConfig->addBodyClass( $this->cleanCssClass( self::PREFIX_LANG . $locale ) );
		}
	}

	/**
	 * CSS class clean-up
	 *
	 * @param  string  $class
	 * @return string
	 */
	private function cleanCssClass( string $class ): string
	{
		return preg_replace( '#[^0-9a-z]+#', '-', strtolower( trim( $class ) ) );
	}
}